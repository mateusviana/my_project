#include <stdio.h>
#include "dice.h"

int main() {
	int n;
	printf("How many faces the dice has? ");
	scanf("%d", &n);
	initializeSeed();                                //inicializa uma seed
	printf("Let's roll the dice: %d\n", rollDice(n));
	return 0;
}
